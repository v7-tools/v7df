# v7df





## Description
v7df is the df utility (`/bin/df`) found in UNIX V7, updated for modern compilers.

## License
v7df is licensed under the MIT license.
